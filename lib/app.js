

var crypto = require('crypto');
var _ = require('lodash');
var moment = require('moment');
var express = require('express');
var swig = require('swig');
var bodyParser = require('body-parser');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;



moment.locale('de');
swig.setDefaults({
    cache: false
});


var allowedUsers = {
    malory: {
        username: 'malory',
        password: "17b0d9850a7e6aa83aca95e23ddd55b9d60b407df24a84ead4897edf8341b4d7"
    }
};
passport.use(new BasicStrategy(function(username, password, done) {

    var hashedPassword = crypto.createHash('sha256').update(password).digest('hex');
    if(username in allowedUsers &&
        allowedUsers[username].password === hashedPassword) {
        return done(null, allowedUsers[username]);
    }

    return done(null, false);
}));

var auth = function() {
    return passport.authenticate('basic', { session: false });
};

var app = express();
app.engine('html', swig.renderFile);
app.set('views', process.cwd() + '/lib/views');
app.set('view engine', 'html');
app.set('view cache', false);
app.set('trust proxy', 1)
app.use(passport.initialize());
app.use(express.static(process.cwd() + '/pub'));

app.get('/', auth(), function(req, res) {

    var baseUrl = req.protocol + '://' + req.get('host');
    var scriptString = '<script id="badScript" type="text/javascript" src="' + baseUrl + '/js/bad.js"></script>';

    res.render('home', {
        scriptString: scriptString,
        sessions: sessions
    });
});

app.get('/reset', function(req, res) {
    sessions = {};
    res.redirect('/');
});

app.get('/js/bad.js', function(req, res) {
    res.sendFile(process.cwd() + '/lib/browser/bad.js');
});

var sessions = {};
app.get('/js/receiver.js', function(req, res) {

    var token = encodeURIComponent(req.query.token);
    var hashedSession = crypto.createHash('sha256').update(token).digest('hex');

    sessions[hashedSession] = {
        token: token,
        refreshed: (new Date).toISOString()
    };

    res.sendFile(process.cwd() + '/lib/browser/receiver.js');
});

app.use(function(req, res){
    res.render('404');
});



var srv = null;
module.exports.start = function(options) {

    srv = app.listen(options.port);
    srv.on('listening', function() {
        console.log('Grundlagen IT-Sicherheit: XSS-Demo');
        console.log('Attacker listening at ' + srv.address().address + ':' + srv.address().port);
        console.log('Go to http://localhost:' + srv.address().port);
    });
}